import { BehaviorSubject } from 'rxjs';

export class GenericSubject<T> {
    private readonly $value: BehaviorSubject<T> = new BehaviorSubject(null);
    constructor(value: T) {
        this.value = value;
    }
    get value(): T {
        return this.$value.getValue();
    }

    set value(survey: T) {
        this.$value.next(survey);
    }

    public async(): BehaviorSubject<T> {
        return this.$value;
    }
}
