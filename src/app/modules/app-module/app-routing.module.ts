import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CERTIFICATES } from '@certificates-module/certificates-routes';


const routes: Routes = [
  { path: '', redirectTo: CERTIFICATES, pathMatch: 'full' },
  {
    path: CERTIFICATES,
    loadChildren: () => import('../certificates/certificates.module').then(m => m.CertificatesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
