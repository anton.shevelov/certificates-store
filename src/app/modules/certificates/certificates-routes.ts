import { Injectable } from '@angular/core';

export const CERTIFICATES = 'certificates';
export const VIEWER = 'viewer';

@Injectable({
    providedIn: 'root'
})
export class CertificatesRouter {
    public VIEWER = `/${CERTIFICATES}/${VIEWER}`;
}
