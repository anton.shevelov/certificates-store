import { Component, OnInit, OnDestroy } from '@angular/core';
import { CertificatesStoreService } from '@certificates-module/services/certificates-store.service';
import { Certificate } from '@interface/certificate';
import { GenericSubject } from 'src/app/classes/generic-subject';

@Component({
    selector: 'app-certificate-list',
    templateUrl: './certificate-list.component.html',
    styleUrls: ['./certificate-list.component.scss'],
})
export class CertificateListComponent implements OnInit {
    public certificates = new GenericSubject<Certificate[]>([]);

    constructor(private css: CertificatesStoreService) {}

    ngOnInit(): void {
        this.certificates = this.css.certificates;
    }
    trackByName(certificate: Certificate) {
        return certificate.commonName;
    }
    selectCertificate(certificate: Certificate): void {
        this.css.selectCertificate(certificate);
    }
}
