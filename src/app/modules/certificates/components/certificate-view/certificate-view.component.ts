import { Component, OnInit } from '@angular/core';
import { CertificatesStoreService } from '@certificates-module/services/certificates-store.service';
import { Certificate } from '@interface/certificate';
import { Subject } from 'rxjs';
import { GenericSubject } from 'src/app/classes/generic-subject';
@Component({
    selector: 'app-certificate-view',
    templateUrl: './certificate-view.component.html',
    styleUrls: ['./certificate-view.component.scss'],
})
export class CertificateViewComponent implements OnInit {
    public objectKeys = Object.keys;
    public selectedCertificate = new GenericSubject<Certificate>(null);

    public fieldsNames = {
        commonName: 'Common name',
        issuerName: 'Issuer CN',
        issuanceDate: 'Valid from',
        expirationDate: 'Valid till',
    };
    constructor(private css: CertificatesStoreService) {}

    ngOnInit(): void {
        this.selectedCertificate = this.css.selectedCertificate;
    }
}
