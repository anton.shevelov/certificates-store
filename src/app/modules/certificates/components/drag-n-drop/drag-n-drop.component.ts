import { Component, OnInit } from '@angular/core';
import { CertificatesParserService } from '@certificates-module/services/certificates-parser.service';

@Component({
    selector: 'app-drag-n-drop',
    templateUrl: './drag-n-drop.component.html',
    styleUrls: ['./drag-n-drop.component.scss'],
})
export class DragNDropComponent implements OnInit {
    constructor(private cps: CertificatesParserService) {}

    ngOnInit(): void {}

    onFileDropped(file: File) {
        this.cps.fileToBlob(file);
    }

    fileBrowseHandler(files) {
        const file = files[0];
        this.cps.fileToBlob(file);
    }
}
