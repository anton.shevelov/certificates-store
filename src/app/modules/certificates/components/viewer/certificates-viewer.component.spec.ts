import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificatesViewerComponent } from './certificates-viewer.component';

describe('CertificatesViewerComponent', () => {
  let component: CertificatesViewerComponent;
  let fixture: ComponentFixture<CertificatesViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificatesViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificatesViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
