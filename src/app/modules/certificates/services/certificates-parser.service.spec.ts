import { TestBed } from '@angular/core/testing';

import { CertificatesParserService } from './certificates-parser.service';

describe('CertificatesParserService', () => {
  let service: CertificatesParserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CertificatesParserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
