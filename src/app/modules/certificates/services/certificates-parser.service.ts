import { Injectable } from '@angular/core';
import * as asn1js from 'asn1js';
import { Certificate } from 'pkijs';
import { Certificate as ICertificate } from '@interface/certificate';
import { CertificatesStoreService } from './certificates-store.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '@material-module/dialog/dialog.component';

@Injectable({
    providedIn: 'root',
})
export class CertificatesParserService {
    constructor(
        private css: CertificatesStoreService,
        public dialog: MatDialog
    ) {}

    fileToBlob(file: File): void {
        const reader = new FileReader();
        reader.onload = (e) => {
            const certificateBuffer = e.target.result;
            this.parseCetrificate(certificateBuffer);
        };
        reader.readAsArrayBuffer(file);
    }

    parseCetrificate(certificateBuffer: string | ArrayBuffer): void {
        try {
            const asn1 = asn1js.fromBER(certificateBuffer);
            const certificate = new Certificate({ schema: asn1.result });

            const certificateInfo: ICertificate = {
                commonName: this.getName(certificate.subject.typesAndValues),
                issuerName: this.getName(certificate.issuer.typesAndValues),
                issuanceDate: certificate.notBefore.value.toString(),
                expirationDate: certificate.notAfter.value.toString(),
            };
            this.css.saveCertificate(certificateInfo);
        } catch (error) {
            this.dialog.open(DialogComponent, {
                width: '250px',
                data: { message: `Can't add certificate` },
            });
        }
        this.css.isAddNewCertificate = false;
    }
    getName(typesAndValues: any[]): string {
        const rdnmap = {
            '2.5.4.6': 'C',
            '2.5.4.10': 'O',
            '2.5.4.11': 'OU',
            '2.5.4.3': 'CN',
            '2.5.4.7': 'L',
            '2.5.4.8': 'S',
            '2.5.4.12': 'T',
            '2.5.4.42': 'GN',
            '2.5.4.43': 'I',
            '2.5.4.4': 'SN',
            '1.2.840.113549.1.9.1': 'E-mail',
        };
        for (const typeAndValue of typesAndValues) {
            let typeval = rdnmap[typeAndValue.type];
            if (typeof typeval === 'undefined') {
                typeval = typeAndValue.type;
            }
            if (typeval === 'CN') {
                const subjval = typeAndValue.value.valueBlock.value;
                return subjval;
            }
        }
        return 'undefined';
    }
}
